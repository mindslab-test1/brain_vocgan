import torch
import torch.nn as nn
from model.discriminator import JCU_Discriminator
from model.multiscale import MultiScaleDiscriminator
import torchaudio

class Hierarchical_JCU_Discriminator(nn.Module):
    def __init__(self, hp):
        super(Hierarchical_JCU_Discriminator, self).__init__()
        self.model = nn.ModuleDict()
        self.downsample = nn.ModuleDict()
        sr = hp.audio.sampling_rate
        for i in range(4):
            self.model[f"disc_{i}"] = JCU_Discriminator(2**(4-i))
            self.downsample[f"down_{i}"] = torchaudio.transforms.Resample(sr, (sr // 2**(4-i)))

        self.multiscale_discriminator = MultiScaleDiscriminator()

    def forward(self, x, mel, resampled_x = None):
        results = []
        multi_scale_out = self.multiscale_discriminator(x, mel) # D0
        i = 0

        for (key, disc), (_, down_) in zip(self.model.items(), self.downsample.items()):
            if resampled_x is not None:
                x_ = resampled_x[i]
            else:
                with torch.no_grad():
                    x_ = down_(x.squeeze(1)).unsqueeze(1)
            results.append(disc(x_, mel)) # [[features, uncond, cond], [features, uncond, cond] ...]
            i = i + 1
        return results, multi_scale_out #  [D1, D2, D3, D4], D0 -> [D01, D02, D03] ,