import torch
import torch.nn as nn
import torch.nn.functional as F

from .identity import Identity
from .res_stack import ResStack
# from res_stack import ResStack

MAX_WAV_VALUE = 32768.0

class Generator(nn.Module):
    def __init__(self, mel_channel):
        super(Generator, self).__init__()

        resolution_num = 5
        upsample_rate = [4, 4, 2, 2, 2, 2]
        mul = 512

        self.k = len(upsample_rate) - resolution_num
        self.mel_channel = mel_channel

        self.first = nn.Sequential(
            nn.ReflectionPad1d(3),
            nn.utils.weight_norm(nn.Conv1d(mel_channel, mul, kernel_size=7, stride=1))
        )

        self.layers = []
        self.res = []
        self.conv_layers = []
        self.skip_mel_layers = []

        for i, r in enumerate(upsample_rate):
            mul = mul // 2

            self.layers.append(
                nn.Sequential(
                    nn.LeakyReLU(0.2),
                    nn.utils.weight_norm(
                        nn.ConvTranspose1d(mul * 2, mul, kernel_size = r * 2, stride = r, padding = r//2)),
                )
            )

            self.res.append(ResStack(mul))

            if self.k <= i:
                self.conv_layers.append(
                    nn.Sequential(
                        nn.LeakyReLU(0.2),
                        nn.ReflectionPad1d(3),
                        nn.utils.weight_norm(nn.Conv1d(mul, 1, kernel_size=7, stride=1)),
                        nn.Tanh(),
                    )
                )
                if self.k == i:
                    self.skip_mel_layers.append(Identity())
                else:
                    self.skip_mel_layers.append(
                        nn.utils.weight_norm(nn.ConvTranspose1d(mel_channel, mul * 2, kernel_size= 2048 // mul, stride= 1024//mul,
                                                            padding=512//mul, output_padding=0))
                    )
            else:
                self.conv_layers.append(Identity())
                self.skip_mel_layers.append(Identity())

        self.layers = nn.ModuleList(self.layers)
        self.res = nn.ModuleList(self.res)

        self.conv_layers = nn.ModuleList(self.conv_layers)
        self.skip_mel_layers = nn.ModuleList(self.skip_mel_layers)

    def forward(self, mel):
        mel = (mel + 5.0) / 5.0 # roughly normalize spectrogram
        output = []
        x = self.first(mel)
        for i,(_block, _res, _conv, _skip) in enumerate(zip(self.layers,self.res,self.conv_layers, self.skip_mel_layers)):
            if i > self.k:
                x = x + _skip(mel)
            x = _block(x)
            x = _res(x)
            if i >= self.k:
                output.append(_conv(x))

        return output #[audio_4, audio_3, audio_2, audio_1, audio_0]

    def eval(self, inference=False):
        super(Generator, self).eval()

        # don't remove weight norm while validation in training loop
        if inference:
            self.remove_weight_norm()

    def remove_weight_norm(self):
        for _block, _res, _conv, _skip in zip(self.layers,self.res,self.conv_layers, self.skip_mel_layers):
            nn.utils.remove_weight_norm(_block[1])
            _res.remove_weight_norm()
            if len(_conv.state_dict) != 0:
                nn.utils.remove_weight_norm(_conv[2])
            if len(_skip.state_dict) != 0:
                nn.utils.remove_weight_norm(_skip)

    def inference(self, mel):
        hop_length = 256
        # pad input mel with zeros to cut artifact
        # see https://github.com/seungwonpark/melgan/issues/8
        zero = torch.full((1, self.mel_channel, 10), -11.5129).to(mel.device)
        mel = torch.cat((mel, zero), dim=2)

        audio = self.forward(mel)[-1]
        audio = audio.squeeze() # collapse all dimension except time axis
        audio = audio[:-(hop_length*10)]
        audio = MAX_WAV_VALUE * audio
        audio = audio.clamp(min=-MAX_WAV_VALUE, max=MAX_WAV_VALUE-1)
        audio = audio.short()

        return audio


'''
    to run this, fix 
    from . import ResStack
    into
    from res_stack import ResStack
'''
if __name__ == '__main__':
    model = Generator(80)

    x = torch.randn(3, 80, 10)
    print(x.shape)

    y = model(x)
    print(y.shape)
    assert y.shape == torch.Size([3, 1, 2560])

    pytorch_total_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    print(pytorch_total_params)
