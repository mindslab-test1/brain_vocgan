import torch
import torch.nn as nn
import torch.nn.functional as F

'''
class Discriminator(nn.Module):
    def __init__(self):
        super(Discriminator, self).__init__()

        self.discriminator = nn.ModuleList([
            nn.Sequential(
                nn.ReflectionPad1d(7),
                nn.utils.weight_norm(nn.Conv1d(1, 16, kernel_size=15, stride=1)),
                nn.LeakyReLU(0.2, inplace=True),
            ),
            nn.Sequential(
                nn.utils.weight_norm(nn.Conv1d(16, 64, kernel_size=41, stride=4, padding=20, groups=4)),
                nn.LeakyReLU(0.2, inplace=True),
            ),
            nn.Sequential(
                nn.utils.weight_norm(nn.Conv1d(64, 256, kernel_size=41, stride=4, padding=20, groups=16)),
                nn.LeakyReLU(0.2, inplace=True),
            ),
            nn.Sequential(
                nn.utils.weight_norm(nn.Conv1d(256, 1024, kernel_size=41, stride=4, padding=20, groups=64)),
                nn.LeakyReLU(0.2, inplace=True),
            ),
            nn.Sequential(
                nn.utils.weight_norm(nn.Conv1d(1024, 1024, kernel_size=41, stride=4, padding=20, groups=256)),
                nn.LeakyReLU(0.2, inplace=True),
            ),
            nn.Sequential(
                nn.utils.weight_norm(nn.Conv1d(1024, 1024, kernel_size=5, stride=1, padding=2)),
                nn.LeakyReLU(0.2, inplace=True),
            ),
            nn.utils.weight_norm(nn.Conv1d(1024, 1, kernel_size=3, stride=1, padding=1)),
        ])

    def forward(self, x, mel):

            returns: (list of 6 features, discriminator score)
            we directly predict score without last sigmoid function
            since we're using Least Squares GAN (https://arxiv.org/abs/1611.04076)
        features = list()
        for module in self.discriminator:
            x = module(x)
            features.append(x)
        return features[:-1], features[-1]
'''

# JCU Discriminator
class JCU_Discriminator(nn.Module):
    def __init__(self, downsample_ratio = 1):
        super(JCU_Discriminator, self).__init__()
        self.mel_conv = nn.Sequential(
            nn.ReflectionPad1d(3),
            nn.utils.weight_norm(nn.Conv1d(80, 128, kernel_size=7, stride=1, )),
            nn.LeakyReLU(0.2, True),
            nn.utils.weight_norm(nn.ConvTranspose1d(128, 128, kernel_size=64 // downsample_ratio,
                                                    stride=32 // downsample_ratio, padding=16 // downsample_ratio)),
            nn.LeakyReLU(0.2, True),
        )
        self.x_conv_list =nn.ModuleList([
            nn.Sequential(
                nn.ReflectionPad1d(7),
                nn.utils.weight_norm(nn.Conv1d(1, 16, kernel_size=15, stride=1)),
                nn.LeakyReLU(0.2, True),
            ),
            nn.Sequential(
                nn.utils.weight_norm(nn.Conv1d(16, 64, kernel_size=41, stride=4, padding=4 * 5, groups=16 // 4, )),
                nn.LeakyReLU(0.2),
            ),
            nn.Sequential(
                nn.utils.weight_norm(nn.Conv1d(64, 128, kernel_size=21, stride=2, padding=2 * 5, groups=64 // 4, )),
                nn.LeakyReLU(0.2),
            )
        ])

        self.cond_conv_list = nn.ModuleList([
            nn.Sequential(
                nn.utils.weight_norm(nn.Conv1d(256, 256, kernel_size=5, stride=1, padding=2)),
                nn.LeakyReLU(0.2, True),
            ),
            nn.utils.weight_norm(nn.Conv1d(256, 1, kernel_size=3, stride=1, padding=1))
        ])

        self.uncond_conv_list = nn.ModuleList([
            nn.Sequential(
                nn.utils.weight_norm(nn.Conv1d(128, 128, kernel_size=5, stride=1, padding=2)),
                nn.LeakyReLU(0.2, True),
            ),
            nn.utils.weight_norm(nn.Conv1d(128, 1, kernel_size=3, stride=1, padding=1))
        ])

    def forward(self, x, mel):
        features = []

        out = self.mel_conv(mel)

        for x_conv in self.x_conv_list:
            x = x_conv(x)
            features.append(x)

        # at validation step, mel output and audio output can get different length.
        length = min(out.size(2), x.size(2))
        out = torch.cat([out[:, :, :length], x[:, :, :length]], dim=1)

        for cond_conv in self.cond_conv_list:
            out = cond_conv(out)
            features.append(out)
        features = features[:-1]

        for uncond_conv in self.uncond_conv_list:
            x = uncond_conv(x)
            features.append(x)
        features = features[:-1]

        return features, x, out

'''
if __name__ == '__main__':
    model = Discriminator()

    x = torch.randn(3, 1, 22050)
    print(x.shape)

    features, score = model(x)
    for feat in features:
        print(feat.shape)
    print(score.shape)

    pytorch_total_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    print(pytorch_total_params)
'''
