# VocGAN
Unofficial PyTorch implementation of [VocGAN Vocoder](https://arxiv.org/pdf/2007.15256)

## Key Features

- VocGAN is as fast as MelGAN, and the sound quality is as good as WaveGlow.
- This repository use identical mel-spectrogram function from [NVIDIA/tacotron2](https://github.com/NVIDIA/tacotron2), so this can be directly used to convert output from NVIDIA's tacotron2 into raw-audio.
- I resampled the audio in the preprocess step, which significantly reduced training cost as a result. 
  (2.80 s/it -> 0.37 it/s)


![](./assets/gd.png)

## Prerequisites

Tested on Python 3.6
```bash
pip install -r requirements.txt
```

## Prepare Dataset

- Download dataset for training. This can be any wav files with sample rate 22050Hz. (e.g. LJSpeech and KSS was used in paper)
- Edit configuration `yaml` file
- preprocess: `python preprocess.py -c config/default.yaml -d [data's root path]`
- The preprocess consists of STFT and resampling. These files are saved in the form of `*.mel` and `*.wav1 ~ 4`

## Train & Tensorboard

- `python trainer.py -c [config yaml file] -n [name of the run]`
  - `cp config/default.yaml config/config.yaml` and then edit `config.yaml`
  - Write down the root path of train/validation files to 2nd/3rd line.
  - Each path should contain pairs of `*.wav` with corresponding (preprocessed) `*.mel` file and resampled `*.wav1 ~ 4`.
  - The data loader parses list of files within the path recursively.
- `tensorboard --logdir logs/`

## Pretrained model

Try with Google Colab: TODO

## Inference

- `python inference.py -p [checkpoint path] -i [input mel path]`

## Results

See audio samples at: http://swpark.me/melgan/.
Model was trained at V100 GPU for 10 days (3200epoch) using LJSpeech-1.1.

![](./assets/lj-tensorboard-v0.3-alpha.png)


## Implementation Authors

- [Kangwook Kim](http://github.com/wookladin)  @ MINDsLab Inc. (full324@snu.ac.kr, kwkim@mindslab.ai)
- [Seungwon Park](http://swpark.me) @ MINDsLab Inc. (yyyyy@snu.ac.kr, swpark@mindslab.ai)

## License

BSD 3-Clause License.

- [utils/stft.py](./utils/stft.py) by Prem Seetharaman (BSD 3-Clause License)
- [datasets/mel2samp.py](./datasets/mel2samp.py) from https://github.com/NVIDIA/waveglow (BSD 3-Clause License)
- [utils/hparams.py](./utils/hparams.py) from https://github.com/HarryVolek/PyTorch_Speaker_Verification (No License specified)
- [utils/stft_loss.py](./utils/stft_loss.py) # Copyright 2019 Tomoki Hayashi #  MIT License (https://opensource.org/licenses/MIT)

