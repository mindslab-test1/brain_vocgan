import os
import math
import tqdm
import torch
import torch.nn as nn
import torch.nn.functional as F
import itertools
import traceback

from model.generator import Generator
from model.hierarchical_discriminator import Hierarchical_JCU_Discriminator
from .utils import get_commit_hash
from .validation import validate
from utils.stft_loss import MultiResolutionSTFTLoss


def train(args, pt_dir, chkpt_path, trainloader, valloader, writer, logger, hp, hp_str):
    model_g = Generator(hp.audio.n_mel_channels).cuda()
    model_d = Hierarchical_JCU_Discriminator(hp).cuda()

    optim_g = torch.optim.Adam(model_g.parameters(),
        lr=hp.train.adam.lr, betas=(hp.train.adam.beta1, hp.train.adam.beta2))
    optim_d = torch.optim.Adam(model_d.parameters(),
        lr=hp.train.adam.lr, betas=(hp.train.adam.beta1, hp.train.adam.beta2))

    githash = get_commit_hash()

    init_epoch = -1
    step = 0

    if chkpt_path is not None:
        logger.info("Resuming from checkpoint: %s" % chkpt_path)
        checkpoint = torch.load(chkpt_path)
        model_g.load_state_dict(checkpoint['model_g'])
        model_d.load_state_dict(checkpoint['model_d'])
        optim_g.load_state_dict(checkpoint['optim_g'])
        optim_d.load_state_dict(checkpoint['optim_d'])
        step = checkpoint['step']
        init_epoch = checkpoint['epoch']

        if hp_str != checkpoint['hp_str']:
            logger.warning("New hparams is different from checkpoint. Will use new.")

        if githash != checkpoint['githash']:
            logger.warning("Code might be different: git hash is different.")
            logger.warning("%s -> %s" % (checkpoint['githash'], githash))

    else:
        logger.info("Starting new training run.")

    # this accelerates training when the size of minibatch is always consistent.
    # if not consistent, it'll horribly slow down.
    torch.backends.cudnn.benchmark = True

    try:
        model_g.train()
        model_d.train()
        stft_criterion = MultiResolutionSTFTLoss()

        for epoch in itertools.count(init_epoch+1):

            if epoch % hp.log.validation_interval == 0:
                with torch.no_grad():
                    validate(hp, args, model_g, model_d, valloader, stft_criterion, writer, step)

            trainloader.dataset.shuffle_mapping()
            loader = tqdm.tqdm(trainloader, desc='Loading train data')
            for (melG, audioG_list), (melD, audioD_list) in loader:

                melG = melG.cuda()
                audioG_list = [audio.cuda() for audio in audioG_list]
                melD = melD.cuda()
                audioD_list = [audio.cuda() for audio in audioD_list]

                # generator
                optim_g.zero_grad()
                fake_audio_list = model_g(melG)

                sc_loss, mag_loss = stft_criterion(
                    fake_audio_list[-1].squeeze(1), audioG_list[-1].squeeze(1))
                stft_loss = sc_loss + mag_loss
                
                
                disc_fake, disc_fake_multiscale= model_d(fake_audio_list[-1], melG, fake_audio_list[:-1])
                disc_real, disc_real_multiscale = model_d(audioG_list[-1], melG, audioG_list[:-1])

                uncond_loss = 0.0
                cond_loss = 0.0
                feat_loss = 0.0

                for (feats_fake, uncond_fake, cond_fake), (feats_real, _, _) in \
                        zip(disc_fake + disc_fake_multiscale, disc_real + disc_real_multiscale):

                    uncond_loss += torch.mean(torch.pow(uncond_fake - 1.0, 2))
                    cond_loss += torch.mean(torch.pow(cond_fake - 1.0, 2))

                    for feat_f, feat_r in zip(feats_fake, feats_real):
                        feat_loss += hp.model.feat_match * \
                                  torch.mean(torch.abs(feat_f - feat_r))

                loss_g = stft_loss + uncond_loss + cond_loss + feat_loss

                loss_g.backward()
                optim_g.step()

                # discriminator
                fake_audio_list = model_g(melD)
                fake_audio_list = [fake_audio.detach() for fake_audio in fake_audio_list]
                loss_d_sum = 0.0
                optim_d.zero_grad()
                disc_fake, disc_fake_multiscale = model_d(fake_audio_list[-1], melD, fake_audio_list[:-1])
                disc_real, disc_real_multiscale = model_d(audioD_list[-1], melD, audioD_list[:-1])

                loss_d = 0.0
                for (_, uncond_fake, cond_fake), (_, uncond_real, cond_real) in \
                        zip(disc_fake + disc_fake_multiscale, disc_real + disc_real_multiscale):
                    loss_d += torch.mean(torch.pow(uncond_real - 1.0, 2))
                    loss_d += torch.mean(torch.pow(uncond_fake, 2))
                    loss_d += torch.mean(torch.pow(cond_real - 1.0, 2))
                    loss_d += torch.mean(torch.pow(cond_fake, 2))

                loss_d.backward()
                optim_d.step()

                step += 1
                # logging
                loss_g = loss_g.item()
                loss_d = loss_d.item()

                if any([loss_g > 1e8, math.isnan(loss_g), loss_d > 1e8, math.isnan(loss_d)]):
                    logger.error("loss_g %.01f loss_d %.01f at step %d!" % (loss_g, loss_d, step))
                    raise Exception("Loss exploded")

                if step % hp.log.summary_interval == 0:
                    writer.log_training(loss_g, loss_d, stft_loss.item(), feat_loss.item(), uncond_loss.item(),
                                        cond_loss.item(), step)
                    loader.set_description("g %.04f d %.04f | step %d" % (loss_g, loss_d, step))

            if epoch % hp.log.save_interval == 0:
                save_path = os.path.join(pt_dir, '%s_%s_%04d.pt'
                    % (args.name, githash, epoch))
                torch.save({
                    'model_g': model_g.state_dict(),
                    'model_d': model_d.state_dict(),
                    'optim_g': optim_g.state_dict(),
                    'optim_d': optim_d.state_dict(),
                    'step': step,
                    'epoch': epoch,
                    'hp_str': hp_str,
                    'githash': githash,
                }, save_path)
                logger.info("Saved checkpoint to: %s" % save_path)

    except Exception as e:
        logger.info("Exiting due to exception: %s" % e)
        traceback.print_exc()
