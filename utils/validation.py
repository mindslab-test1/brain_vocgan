import tqdm
import torch
import torchaudio

def validate(hp, args, generator, discriminator, valloader, stft_loss, writer, step):
    generator.eval()
    discriminator.eval()
    torch.backends.cudnn.benchmark = False

    loader = tqdm.tqdm(valloader, desc='Validation loop')
    loss_g_sum = 0.0
    loss_d_sum = 0.0
    for mel, audio_list in loader:
        mel = mel.cuda()
        audio_list = [audio.cuda() for audio in audio_list]
        audio = audio_list[-1]

        fake_audio_list = generator(mel)
        fake_audio = fake_audio_list[-1][:,:,:audio.size(2)]

        sc_loss, mag_loss = stft_loss(fake_audio.squeeze(1), audio.squeeze(1))
        loss_g = sc_loss + mag_loss

        disc_fake, disc_fake_multiscale = discriminator(fake_audio, mel, resampled_x = fake_audio_list[:-1])
        disc_real, disc_real_multiscale = discriminator(audio, mel, resampled_x = audio_list[:-1])

        for (feats_fake, uncond_fake, cond_fake), (feats_real, _, _) in \
                zip(disc_fake + disc_fake_multiscale, disc_real + disc_real_multiscale):
            loss_g += torch.mean(torch.pow(uncond_fake - 1.0, 2))
            loss_g += torch.mean(torch.pow(cond_fake - 1.0, 2))

            for feat_f, feat_r in zip(feats_fake, feats_real):
                feat_len = min(feat_f.size(2), feat_r.size(2))
                loss_g += hp.model.feat_match * \
                          torch.mean(torch.abs(feat_f[:, :, :feat_len] - feat_r[:, :, :feat_len]))


        loss_d = 0.0
        for (_, uncond_fake, cond_fake), (_, uncond_real, cond_real) in \
                zip(disc_fake + disc_fake_multiscale, disc_real + disc_real_multiscale):
            loss_d += torch.mean(torch.pow(uncond_real - 1.0, 2))
            loss_d += torch.mean(torch.pow(uncond_fake, 2))
            loss_d += torch.mean(torch.pow(cond_real - 1.0, 2))
            loss_d += torch.mean(torch.pow(cond_fake, 2))

        loss_g_sum += loss_g.item()
        loss_d_sum += loss_d.item()

    loss_g_avg = loss_g_sum / len(valloader.dataset)
    loss_d_avg = loss_d_sum / len(valloader.dataset)

    audio = audio[0][0].cpu().detach().numpy()
    fake_audio = fake_audio[0][0].cpu().detach().numpy()

    writer.log_validation(loss_g_avg, loss_d_avg, generator, discriminator, audio, fake_audio, step)

    torch.backends.cudnn.benchmark = True
